<?php 
	// var_dump($_GET);
	$name = $_GET['name'];

	$items = file_get_contents("../assets/lib/products.json");

	$items_array = json_decode($items, true);

	foreach($items_array as $index => $indiv_item){

		if($indiv_item['name']=== $name){
			unset($items_array[$index]);
		}
	}

	// var_dump($items_array);
	$to_write = fopen("../assets/lib/products.json", "w");

	fwrite($to_write, json_encode($items_array, JSON_PRETTY_PRINT));

	fclose($to_write);

	header("Location: ".$_SERVER['HTTP_REFERER']);

?>