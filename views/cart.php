<?php 
	require "../partials/template.php";

	function get_body_contents(){

?>
	<h1 class="text-center py-3">Cart</h1>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<th>Item Name:</th>
						<th>Item Price:</th>
						<th>Item Quantity:</th>
						<th>Subtotal</th>
						<th></th>
					</thead>
					<tbody>
						<?php 
						session_start();
						$items = file_get_contents("../assets/lib/products.json");	
						$items_array = json_decode($items, true);
						$total = 0;
						if (isset($_SESSION['cart'])) {
							foreach($_SESSION['cart'] as $name => $quantity) {
								foreach($items_array as $indiv_item){
									if($name == $indiv_item['name']){
										$subtotal = $indiv_item['price']*$quantity;
										$total += $subtotal;
										?>
										<tr>
										<td><?php echo $name ?></td>
										<td><?php echo $indiv_item['price']?></td>
											<td>
												<form action="../controllers/add-to-cart-process.php" method="POST">
													<input type="hidden" name="name" value="<?php echo $name ?>">
													<input type="hidden" name="fromCartPage" value="fromCartPage">
													<div class="input-group">
													<input type="number" name="quantity" value="<?php echo $quantity?>" class="form-control" >
													<div class="input-group-append">
													<button class="btn btn-sm btn-success" type="submit">Update</button>
													</div>
													</div>
												</form>

											</td>

										
										<td><?php echo $subtotal ?></td>
										<td>
											<a href="../controllers/remove-from-cart-process.php?name=<?php echo $name ?>" class="btn btn-danger">Remove From Cart</a>
										</td>
										</tr>
										
										<?php
									}
								}
							}
						}


						?>
						<tr class="bg-info">
							<td></td>
							<td></td>
							<td>Total:</td>
							<td><?php echo number_format($total, 2, ".", ",")?></td>
							<td>
								<a href="../controllers/empty-cart-process.php" class="btn btn-danger">Empty Cart</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php
	}
?>