<?php 
	require "../partials/template.php";
	function get_body_contents(){
?>

<h1 class="text-center py-3">Featured Menu Items</h1>

<div class="container">
	<div class="row">
		<?php 
			// get all the products from the file
			$products = file_get_contents("../assets/lib/products.json");
			// Transforms json into a readable format by PHP
			$products_array = json_decode($products, true);
			foreach($products_array as $indiv_product){
		?>	
		<div class="col-lg-4 py-2">
			<div class="card">
				<img class="card-img-top" height="400px" src="../assets/lib/<?php echo $indiv_product['image']?>" alt="">
			</div>
			<div class="card-body">
				<h5 class="card-title"><?php echo $indiv_product['name']?></h5>
				<p class="card-text">Price: Php <?php echo $indiv_product['price']?></p>
				<p class="card-text">Description: <?php echo $indiv_product['description']?></p>
			</div>
			<div class="card-footer text-center">
				<a href="../controllers/delete-item-process.php?name=<?php echo $indiv_product['name']?>" class="btn btn-danger">Delete Item</a>
			</div>	
			<div class="card-footer">
				<form action="../controllers/add-to-cart-process.php" method="POST">
					<input type="hidden" name="name" class="form-control" value="<?php echo $indiv_product['name']?>">
					<input type="number" name="quantity" value="1" class="form-control">
					<button type="submit" class="btn btn-info btn-block">+ Add to Cart</button>
				</form>

			</div>

		</div>

		<?php
			}
		?>
	</div>	
</div>
<?php
	}
?>